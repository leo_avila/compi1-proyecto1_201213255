package EDD;

public class node_b {

    public String name, path;
    public node_b next, prev;

    public node_b(String n, String p, node_b l, node_b r) {
        this.name = n;
        this.path = p;
        this.next = l;
        this.prev = r;
    }

    public node_b(String n, String p) {
        this(n, p, null, null);
    }
}
