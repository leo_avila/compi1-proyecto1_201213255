package EDD;

public class dlink_b {

    private node_b first, last;

    public dlink_b() {
        first = last = null;
    }

    public boolean empty() {
        return first == null;
    }

    public void addItem(String n, String p) {
        if (!empty()) {
            first = new node_b(n, p, first, null);
            first.next.prev = last;
        } else {
            first = last = new node_b(n, p);
        }
    }

    public String showPath(String n) {
        node_b aux = first;
        if (!empty()) {
            while(!aux.name.equals(n)){
                if(aux.next != null){
                    aux = aux.next;
                }else{
                    return "NULO";
                }
            }
            return aux.path;
        } else {
            return "NULO";
        }
    }
    
    public void print(){
        node_b aux = first;
        if(!empty()){
            while(aux.next != null){
                System.out.println(aux.name + " : " + aux.path);
                aux = aux.next;
            }
        } else {
            System.out.println("VACIO");
        }
    }
}
