
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20150326 (SVN rev 63)
//----------------------------------------------------

package PARSER1;

/** CUP generated class containing symbol constants. */
public class tabla_simbolos {
  /* terminals */
  public static final int ts_config = 5;
  public static final int te_config = 2;
  public static final int t_coma = 14;
  public static final int tpr_crecimiento = 13;
  public static final int t_corchete_s = 16;
  public static final int ts_disenio = 7;
  public static final int terrorlex = 22;
  public static final int t_nombre = 18;
  public static final int tpr_puntos = 10;
  public static final int tpr_tipo = 11;
  public static final int t_path = 19;
  public static final int t_numero = 20;
  public static final int EOF = 0;
  public static final int t_corchete_e = 15;
  public static final int te_disenio = 4;
  public static final int ts_fondo = 6;
  public static final int t_igual = 17;
  public static final int tpr_nombre = 8;
  public static final int error = 1;
  public static final int t_pyc = 21;
  public static final int tpr_tiempo = 12;
  public static final int tpr_path = 9;
  public static final int te_fondo = 3;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "te_config",
  "te_fondo",
  "te_disenio",
  "ts_config",
  "ts_fondo",
  "ts_disenio",
  "tpr_nombre",
  "tpr_path",
  "tpr_puntos",
  "tpr_tipo",
  "tpr_tiempo",
  "tpr_crecimiento",
  "t_coma",
  "t_corchete_e",
  "t_corchete_s",
  "t_igual",
  "t_nombre",
  "t_path",
  "t_numero",
  "t_pyc",
  "terrorlex"
  };
}

