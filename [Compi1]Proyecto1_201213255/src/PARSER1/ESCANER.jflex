package PARSER1;

import java_cup.runtime.Symbol;

%%

T_NOMBRE	=	([a-zA-Z]+[0-9]*)+
T_PATH		= 	[\"]([/]?[a-zA-Z]+[0-9]*)+[.]([j][p][g]|[p][n][g])[\"]
T_NUMERO	= 	[-]?[0-9]+

%cupsym tabla_simbolos
%class lexico
%cup
%public
%unicode
%line
%column
%char
%ignorecase
%%

/* PALABRAS RESERVADAS */
"<configuracion>"	{return new Symbol(tabla_simbolos.te_config, yycolumn,yyline, new String(yytext())); }
"<fondo>" 			{return new Symbol(tabla_simbolos.te_fondo, yycolumn,yyline, new String(yytext())); }
"<disenio>" 		{return new Symbol(tabla_simbolos.te_disenio, yycolumn,yyline, new String(yytext())); }
"</configuracion>" 	{return new Symbol(tabla_simbolos.ts_config, yycolumn,yyline, new String(yytext())); }
"</fondo>" 			{return new Symbol(tabla_simbolos.ts_fondo, yycolumn,yyline, new String(yytext())); }
"</disenio>" 		{return new Symbol(tabla_simbolos.ts_disenio, yycolumn,yyline, new String(yytext())); }
"nombre" 			{return new Symbol(tabla_simbolos.tpr_nombre, yycolumn,yyline, new String(yytext())); }
"path" 				{return new Symbol(tabla_simbolos.tpr_path, yycolumn,yyline, new String(yytext())); }
"puntos" 			{return new Symbol(tabla_simbolos.tpr_puntos, yycolumn,yyline, new String(yytext())); }
"tipo" 				{return new Symbol(tabla_simbolos.tpr_tipo, yycolumn,yyline, new String(yytext())); }
"tiempo"		 	{return new Symbol(tabla_simbolos.tpr_tiempo, yycolumn,yyline, new String(yytext())); }
"crecimiento" 		{return new Symbol(tabla_simbolos.tpr_crecimiento, yycolumn,yyline, new String(yytext())); }

/* SIMBOLOS Y SIGNOS DE PUNTUACION */
","					{return new Symbol(tabla_simbolos.t_coma, yycolumn,yyline, new String(yytext())); }
"["					{return new Symbol(tabla_simbolos.t_corchete_e, yycolumn,yyline, new String(yytext())); }
"]"					{return new Symbol(tabla_simbolos.t_corchete_s, yycolumn,yyline, new String(yytext())); }
"="					{return new Symbol(tabla_simbolos.t_igual, yycolumn,yyline, new String(yytext())); }
";"					{return new Symbol(tabla_simbolos.t_pyc, yycolumn,yyline, new String(yytext())); }

/* TEXTO DE NOMBRE Y PATH */
{T_NOMBRE}			{return new Symbol(tabla_simbolos.t_nombre, yycolumn,yyline, new String(yytext())); }
{T_PATH}			{return new Symbol(tabla_simbolos.t_path, yycolumn,yyline, new String(yytext())); }

/* NUMEROS */
{T_NUMERO} 			{return new Symbol(tabla_simbolos.t_numero, yycolumn,yyline,new String(yytext()));}

/* BLANCOS */
[ \t\r\f\n]+ 		{ /* Se ignoran */}

/* CUAQUIER OTRO */
. 					{ return new Symbol(tabla_simbolos.terrorlex, yycolumn,yyline, new String(yytext())); }