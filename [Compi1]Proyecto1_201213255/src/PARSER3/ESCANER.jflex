package PARSER3;

import java_cup.runtime.Symbol;

%%

T_NOMBRE	=	([-_]*[0-9]*[a-zA-Z]+[-_]*[0-9]*)+

%cupsym tabla_simbolos
%class lexico
%cup
%public
%unicode
%line
%column
%char
%ignorecase
%%

/* PALABRAS RESERVADAS */
"estrategias" 			{return new Symbol(tabla_simbolos.tpr_estrategias, yycolumn,yyline, new String(yytext())); }
"movimientos" 			{return new Symbol(tabla_simbolos.tpr_movimientos, yycolumn,yyline, new String(yytext())); }
"izquierda" 			{return new Symbol(tabla_simbolos.t_izquierda, yycolumn,yyline, new String(yytext())); }
"derecha" 				{return new Symbol(tabla_simbolos.t_derecha, yycolumn,yyline, new String(yytext())); }
"arriba" 				{return new Symbol(tabla_simbolos.t_arriba, yycolumn,yyline, new String(yytext())); }
"abajo" 				{return new Symbol(tabla_simbolos.t_abajo, yycolumn,yyline, new String(yytext())); }
"atras" 				{return new Symbol(tabla_simbolos.t_atras, yycolumn,yyline, new String(yytext())); }

/* SIMBOLOS Y SIGNOS DE PUNTUACION */
","					{return new Symbol(tabla_simbolos.t_coma, yycolumn,yyline, new String(yytext())); }
":"					{return new Symbol(tabla_simbolos.t_dpuntos, yycolumn,yyline, new String(yytext())); }
"{"					{return new Symbol(tabla_simbolos.t_llave_e, yycolumn,yyline, new String(yytext())); }
"}"					{return new Symbol(tabla_simbolos.t_llave_s, yycolumn,yyline, new String(yytext())); }
"["					{return new Symbol(tabla_simbolos.t_corchete_e, yycolumn,yyline, new String(yytext())); }
"]"					{return new Symbol(tabla_simbolos.t_corchete_s, yycolumn,yyline, new String(yytext())); }


/* TEXTO DE NOMBRE Y PATH */
{T_NOMBRE}			{return new Symbol(tabla_simbolos.t_nombre, yycolumn,yyline, new String(yytext())); }

/* BLANCOS */
[ \t\r\f\n]+ 		{ /* Se ignoran */}

/* CUAQUIER OTRO */
. 					{ return new Symbol(tabla_simbolos.terrorlex, yycolumn,yyline, new String(yytext())); }