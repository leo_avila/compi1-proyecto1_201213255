package PARSER2;

import java_cup.runtime.Symbol;

%%

T_NOMBRE	=	([-_]*[0-9]*[a-zA-Z]+[-_]*[0-9]*)+
T_NUMERO	= 	[0-9]+
T_ITEM 		=	([-_]*[0-9]*[a-zA-Z]+[-_]*[0-9]*)+[(][0-9]+[.]?[.]?[.]?[0-9]*[,][0-9]+[.]?[.]?[.]?[0-9]*[)]

%cupsym tabla_simbolos
%class lexico
%cup
%public
%unicode
%line
%column
%char
%ignorecase
%%

/* PALABRAS RESERVADAS */
"<paredes>" 	{return new Symbol(tabla_simbolos.te_paredes, yycolumn,yyline, new String(yytext())); }
"<extras>" 		{return new Symbol(tabla_simbolos.te_extras, yycolumn,yyline, new String(yytext())); }
"<manzana>" 	{return new Symbol(tabla_simbolos.te_manzana, yycolumn,yyline, new String(yytext())); }
"<bloque>" 		{return new Symbol(tabla_simbolos.te_bloque, yycolumn,yyline, new String(yytext())); }
"<bonus>" 		{return new Symbol(tabla_simbolos.te_bonus, yycolumn,yyline, new String(yytext())); }
"<bomba>" 		{return new Symbol(tabla_simbolos.te_bomba, yycolumn,yyline, new String(yytext())); }
"<estrella>" 	{return new Symbol(tabla_simbolos.te_estrella, yycolumn,yyline, new String(yytext())); }
"<veneno>" 		{return new Symbol(tabla_simbolos.te_veneno, yycolumn,yyline, new String(yytext())); }
"</paredes>" 	{return new Symbol(tabla_simbolos.ts_paredes, yycolumn,yyline, new String(yytext())); }
"</extras>" 	{return new Symbol(tabla_simbolos.ts_extras, yycolumn,yyline, new String(yytext())); }
"</manzana>" 	{return new Symbol(tabla_simbolos.ts_manzana, yycolumn,yyline, new String(yytext())); }
"</bloque>" 	{return new Symbol(tabla_simbolos.ts_bloque, yycolumn,yyline, new String(yytext())); }
"</bonus>" 		{return new Symbol(tabla_simbolos.ts_bonus, yycolumn,yyline, new String(yytext())); }
"</bomba>" 		{return new Symbol(tabla_simbolos.ts_bomba, yycolumn,yyline, new String(yytext())); }
"</estrella>" 	{return new Symbol(tabla_simbolos.ts_estrella, yycolumn,yyline, new String(yytext())); }
"</veneno>" 	{return new Symbol(tabla_simbolos.ts_veneno, yycolumn,yyline, new String(yytext())); }
"escenario" 	{return new Symbol(tabla_simbolos.tpr_escenario, yycolumn,yyline, new String(yytext())); }
"fondo" 		{return new Symbol(tabla_simbolos.tpr_fondo, yycolumn,yyline, new String(yytext())); }
"ancho" 		{return new Symbol(tabla_simbolos.tpr_ancho, yycolumn,yyline, new String(yytext())); }
"alto" 			{return new Symbol(tabla_simbolos.tpr_alto, yycolumn,yyline, new String(yytext())); }
          
 

/* SIMBOLOS Y SIGNOS DE PUNTUACION */
"<"					{return new Symbol(tabla_simbolos.t_brackets_e, yycolumn,yyline, new String(yytext())); }
">"					{return new Symbol(tabla_simbolos.t_brackets_s, yycolumn,yyline, new String(yytext())); }
"/"					{return new Symbol(tabla_simbolos.t_diagonal, yycolumn,yyline, new String(yytext())); }
"="					{return new Symbol(tabla_simbolos.t_igual, yycolumn,yyline, new String(yytext())); }
";"					{return new Symbol(tabla_simbolos.t_pyc, yycolumn,yyline, new String(yytext())); }

/* TEXTO DE NOMBRE */
{T_NOMBRE}			{return new Symbol(tabla_simbolos.t_nombre, yycolumn,yyline, new String(yytext())); }

/* TEXTO DE PATH */
([-_]*[0-9]*[a-zA-Z]+[-_]*[0-9]*)+[(][0-9]+[.]?[.]?[.]?[0-9]*[,][0-9]+[.]?[.]?[.]?[0-9]*[)]			{return new Symbol(tabla_simbolos.t_item, yycolumn,yyline, new String(yytext())); }


/* NUMEROS */
[0-9]+				{return new Symbol(tabla_simbolos.t_numero, yycolumn,yyline,new String(yytext()));}

/* BLANCOS */
[ \t\r\f\n]+ 		{ /* Se ignoran */}

/* CUAQUIER OTRO */
. 					{ return new Symbol(tabla_simbolos.terrorlex, yycolumn,yyline, new String(yytext())); }