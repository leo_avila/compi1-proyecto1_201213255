
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20150326 (SVN rev 63)
//----------------------------------------------------

package PARSER2;

/** CUP generated class containing symbol constants. */
public class tabla_simbolos {
  /* terminals */
  public static final int ts_estrella = 17;
  public static final int te_bonus = 7;
  public static final int ts_bomba = 16;
  public static final int t_brackets_e = 23;
  public static final int ts_veneno = 18;
  public static final int t_diagonal = 25;
  public static final int terrorlex = 2;
  public static final int ts_extras = 12;
  public static final int te_manzana = 5;
  public static final int t_nombre = 27;
  public static final int ts_bloque = 14;
  public static final int te_extras = 4;
  public static final int te_bloque = 6;
  public static final int ts_paredes = 11;
  public static final int t_numero = 29;
  public static final int ts_manzana = 13;
  public static final int EOF = 0;
  public static final int te_bomba = 8;
  public static final int t_igual = 26;
  public static final int tpr_alto = 22;
  public static final int te_paredes = 3;
  public static final int tpr_escenario = 19;
  public static final int error = 1;
  public static final int t_item = 28;
  public static final int te_veneno = 10;
  public static final int t_pyc = 30;
  public static final int te_estrella = 9;
  public static final int ts_bonus = 15;
  public static final int t_brackets_s = 24;
  public static final int tpr_fondo = 20;
  public static final int tpr_ancho = 21;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "terrorlex",
  "te_paredes",
  "te_extras",
  "te_manzana",
  "te_bloque",
  "te_bonus",
  "te_bomba",
  "te_estrella",
  "te_veneno",
  "ts_paredes",
  "ts_extras",
  "ts_manzana",
  "ts_bloque",
  "ts_bonus",
  "ts_bomba",
  "ts_estrella",
  "ts_veneno",
  "tpr_escenario",
  "tpr_fondo",
  "tpr_ancho",
  "tpr_alto",
  "t_brackets_e",
  "t_brackets_s",
  "t_diagonal",
  "t_igual",
  "t_nombre",
  "t_item",
  "t_numero",
  "t_pyc"
  };
}

