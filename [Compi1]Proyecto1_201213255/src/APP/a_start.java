package APP;

import EDD.dlink_b;
import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultStyledDocument;

public class a_start extends javax.swing.JFrame {

    /*  VARIABLES OF MATRIX OF JLABELS  */
    public static int row, column;
    public static String backg_name;
    //private final int[][] logical_m;
    private JLabel[][] physical_m;

    /*      GAMES VARIABLES (FILE 1)    */
    public static LinkedList<String> background_n = new LinkedList<>();
    public static LinkedList<String> background_p = new LinkedList<>();
    public static dlink_b nb = new dlink_b();

    /*      GAMES VARIABLES (FILE 3)    */
    public static LinkedList<String> snake_moves = new LinkedList<>();
    public static LinkedList<String> snake_strat = new LinkedList<>();

    /* VARIABLES DE MODIFICACIÓN DE ARCHIVO */
    FileNameExtensionFilter Filtro = new FileNameExtensionFilter("Archivo .snake", "snake");
    JFileChooser Seleccionado = new JFileChooser();
    File Archivo;
    File opened_file_f;
    boolean opened_file_b = false;
    file_class CA = new file_class();
    DefaultStyledDocument doc = new DefaultStyledDocument();

    public a_start() {
        initComponents();
    }

    /*  -   METHODS CREATED BY THE USER   -   */
    //  -   -   MATRIX OF JLABELS ON PANEL
    void graph_matrix(int x, int y) {
        physical_m = new JLabel[y][x];
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                physical_m[i][j] = new JLabel();
                physical_m[i][j].setBounds(((i * 25)), ((j * 25)), 25, 25);
                physical_m[i][j].setBorder(BorderFactory.createLineBorder(Color.gray));
                physical_m[i][j].setVisible(true);
                physical_m[i][j].revalidate();

                playing_panel.add(physical_m[i][j]);
            }
        }
        repaint();
    }

    /*  -   METHODS CREATED BY THE PROGRAM (NETBEANS)   -   */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        area_texto = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        moves_label = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        strat_label = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        backg_label = new javax.swing.JLabel();
        backg_path_label = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        playing_panel = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuBar1 = new javax.swing.JMenuBar();
        bt_file = new javax.swing.JMenu();
        bt_new = new javax.swing.JMenuItem();
        bt_open_file = new javax.swing.JMenuItem();
        bt_save = new javax.swing.JMenuItem();
        bt_save_as = new javax.swing.JMenuItem();
        bt_exit = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        bt_compile_1 = new javax.swing.JMenuItem();
        bt_compile_2 = new javax.swing.JMenuItem();
        bt_compile_3 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        area_texto.setColumns(20);
        area_texto.setRows(5);
        jScrollPane1.setViewportView(area_texto);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Panel de Edición", jPanel1);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("D E T A L L E S   D E   J U E G O");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Movimiento actual:");

        moves_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        moves_label.setText("Movimientos");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Estrategia en juego:");

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        strat_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        strat_label.setText("Nombre de Estrategia");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Fondo de Escenario:");

        backg_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        backg_label.setText("Nombre de Fondo");

        backg_path_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        backg_path_label.setText("Path de Archivo");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Path de Imagen:");

        jButton2.setText("jButton1");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(174, 174, 174)
                        .addComponent(jLabel2))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel6))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(backg_path_label)
                                .addComponent(backg_label))
                            .addGap(21, 21, 21))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jButton1)
                            .addGap(89, 89, 89)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel3))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(moves_label)
                                .addComponent(strat_label)))))
                .addContainerGap(117, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(187, 187, 187))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(225, 225, 225)
                        .addComponent(jButton1))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(backg_label))
                        .addGap(38, 38, 38)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(backg_path_label)
                            .addComponent(jLabel4))
                        .addGap(36, 36, 36)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(strat_label))
                        .addGap(38, 38, 38)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(moves_label)
                            .addComponent(jLabel3))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(78, 78, 78))
        );

        jTabbedPane1.addTab("Panel de Juego", jPanel3);

        playing_panel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        javax.swing.GroupLayout playing_panelLayout = new javax.swing.GroupLayout(playing_panel);
        playing_panel.setLayout(playing_panelLayout);
        playing_panelLayout.setHorizontalGroup(
            playing_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 592, Short.MAX_VALUE)
        );
        playing_panelLayout.setVerticalGroup(
            playing_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        bt_file.setText("  Archivo  ");
        bt_file.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N

        bt_new.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_new.setText("Nuevo");
        bt_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_newActionPerformed(evt);
            }
        });
        bt_file.add(bt_new);

        bt_open_file.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_open_file.setText("Abrir");
        bt_open_file.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_open_fileActionPerformed(evt);
            }
        });
        bt_file.add(bt_open_file);

        bt_save.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_save.setText("Guardar");
        bt_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_saveActionPerformed(evt);
            }
        });
        bt_file.add(bt_save);

        bt_save_as.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_save_as.setText("Guardar como");
        bt_save_as.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_save_asActionPerformed(evt);
            }
        });
        bt_file.add(bt_save_as);

        bt_exit.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_exit.setText("Salir");
        bt_exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_exitActionPerformed(evt);
            }
        });
        bt_file.add(bt_exit);

        jMenuBar1.add(bt_file);

        jMenu5.setText("  Compilar  ");
        jMenu5.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N

        bt_compile_1.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_compile_1.setText("Archivo 1");
        bt_compile_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_compile_1ActionPerformed(evt);
            }
        });
        jMenu5.add(bt_compile_1);

        bt_compile_2.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_compile_2.setText("Archivo 2");
        bt_compile_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_compile_2ActionPerformed(evt);
            }
        });
        jMenu5.add(bt_compile_2);

        bt_compile_3.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        bt_compile_3.setText("Archivo 3");
        bt_compile_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_compile_3ActionPerformed(evt);
            }
        });
        jMenu5.add(bt_compile_3);

        jMenuBar1.add(jMenu5);

        jMenu4.setText("  Juego  ");
        jMenu4.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N

        jMenuItem2.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jMenuItem2.setText("Mostrar Tablero");
        jMenu4.add(jMenuItem2);

        jMenuItem4.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jMenuItem4.setText("Ejecutar Juego");
        jMenu4.add(jMenuItem4);

        jMenuBar1.add(jMenu4);

        jMenu2.setText("  Ayuda  ");
        jMenu2.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N

        jMenuItem1.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jMenuItem1.setText("Manual Técnico");
        jMenu2.add(jMenuItem1);

        jMenuItem3.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jMenuItem3.setText("Manual de Usuario");
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("  Acerca de  ");
        jMenu3.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 620, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(playing_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addComponent(playing_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bt_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_newActionPerformed
        area_texto.setText("");
        opened_file_b = false;
        opened_file_f = null;
    }//GEN-LAST:event_bt_newActionPerformed

    private void bt_open_fileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_open_fileActionPerformed
        Seleccionado.setFileFilter(Filtro);
        if (Seleccionado.showDialog(this, "Abrir Archivo") == JFileChooser.APPROVE_OPTION) {
            Archivo = Seleccionado.getSelectedFile();
            if (Archivo.canRead()) {
                if (Archivo.getName().endsWith("snake")) {
                    String Contenido = CA.AbrirArchivo(Archivo);
                    area_texto.setText("");
                    area_texto.setText(Contenido);
                }
            }
        }
    }//GEN-LAST:event_bt_open_fileActionPerformed

    private void bt_exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_exitActionPerformed
        JOptionPane.showMessageDialog(null, "Esta apunto de salir del programa");
        System.exit(0);
    }//GEN-LAST:event_bt_exitActionPerformed

    private void bt_save_asActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_save_asActionPerformed
        Seleccionado.setFileFilter(Filtro);
        if (Seleccionado.showDialog(null, "Guardar Archivo") == JFileChooser.APPROVE_OPTION) {
            Archivo = Seleccionado.getSelectedFile();
            if (Archivo.getName().endsWith("snake")) {
                String contenido = area_texto.getText();
                String respuesta = CA.GuardarArchivo(Archivo, contenido);
                if (respuesta != null) {
                    JOptionPane.showMessageDialog(null, respuesta);
                } else {
                    JOptionPane.showMessageDialog(null, "Error guardando el archivo");
                }
            }
        }
        opened_file_b = true;
        opened_file_f = Archivo;
    }//GEN-LAST:event_bt_save_asActionPerformed

    private void bt_compile_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_compile_1ActionPerformed
        if ("".equals(area_texto.getText())) {
            JOptionPane.showMessageDialog(null, "El área de texto se encuentra vacia.");
        } else {
            try {
                new PARSER1.sintactico(new PARSER1.lexico(new StringReader(area_texto.getText()))).parse();
            } catch (Exception ex) {
                Logger.getLogger(a_start.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        backg_label.setText(background_n.peek());
        backg_path_label.setText(background_p.peek());
/*        
        System.out.println(background_n.peek() + "  :  " + background_p.peek());
        
        nb.addItem(background_n.pop(), background_p.pop());
        
        System.out.println(background_n.peek() + "  :  " + background_p.peek());
        
        
        System.out.println(nb.showPath("fondo_1"));
*/
        
    }//GEN-LAST:event_bt_compile_1ActionPerformed

    private void bt_compile_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_compile_2ActionPerformed
        if ("".equals(area_texto.getText())) {
            JOptionPane.showMessageDialog(null, "El área de texto se encuentra vacia.");
        } else {
            try {
                new PARSER2.sintactico(new PARSER2.lexico(new StringReader(area_texto.getText()))).parse();
            } catch (Exception ex) {
                Logger.getLogger(a_start.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println(background_p.get(background_n.indexOf(backg_name)));

        //  -   -   DRAWING THE MATRIX
        graph_matrix(row, column);
        backg_label.setText(backg_name);
        backg_path_label.setText(background_p.get(background_n.indexOf(backg_name)));

    }//GEN-LAST:event_bt_compile_2ActionPerformed

    private void bt_compile_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_compile_3ActionPerformed
        if ("".equals(area_texto.getText())) {
            JOptionPane.showMessageDialog(null, "El área de texto se encuentra vacia.");
        } else {
            try {
                new PARSER3.sintactico(new PARSER3.lexico(new StringReader(area_texto.getText()))).parse();
            } catch (Exception ex) {
                Logger.getLogger(a_start.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        strat_label.setText(snake_strat.peek());
        moves_label.setText("   -   -   -   ");
    }//GEN-LAST:event_bt_compile_3ActionPerformed

    private void bt_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_saveActionPerformed
        if (opened_file_b == false) {
            Seleccionado.setFileFilter(Filtro);
            if (Seleccionado.showDialog(null, "Guardar Archivo") == JFileChooser.APPROVE_OPTION) {
                Archivo = Seleccionado.getSelectedFile();
                if (Archivo.getName().endsWith("snake")) {
                    String contenido = area_texto.getText();
                    String respuesta = CA.GuardarArchivo(Archivo, contenido);
                    if (respuesta != null) {
                        JOptionPane.showMessageDialog(null, respuesta);
                    } else {
                        JOptionPane.showMessageDialog(null, "Error guardando el archivo");
                    }
                }
            }
            opened_file_b = true;
            opened_file_f = Archivo;
        } else {
            if (Archivo.getName().endsWith("snake")) {
                String contenido = area_texto.getText();
                String respuesta = CA.GuardarArchivo(opened_file_f, contenido);
                if (respuesta != null) {
                    JOptionPane.showMessageDialog(null, respuesta);
                } else {
                    JOptionPane.showMessageDialog(null, "Error guardando el archivo");
                }
            }
        }
    }//GEN-LAST:event_bt_saveActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //moves_label.setText(snake_moves.pop());    
        nb.print();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        System.out.println(background_p.get(background_n.indexOf("fondo_1")));
    }//GEN-LAST:event_jButton2ActionPerformed

    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(a_start.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(() -> {
            new a_start().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea area_texto;
    private javax.swing.JLabel backg_label;
    private javax.swing.JLabel backg_path_label;
    private javax.swing.JMenuItem bt_compile_1;
    private javax.swing.JMenuItem bt_compile_2;
    private javax.swing.JMenuItem bt_compile_3;
    private javax.swing.JMenuItem bt_exit;
    private javax.swing.JMenu bt_file;
    private javax.swing.JMenuItem bt_new;
    private javax.swing.JMenuItem bt_open_file;
    private javax.swing.JMenuItem bt_save;
    private javax.swing.JMenuItem bt_save_as;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel moves_label;
    private javax.swing.JPanel playing_panel;
    private javax.swing.JLabel strat_label;
    // End of variables declaration//GEN-END:variables
}
