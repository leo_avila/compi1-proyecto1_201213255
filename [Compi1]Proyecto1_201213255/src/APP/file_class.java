package APP;

import java.io.*;

public class file_class {
    FileInputStream Entrada;
    FileOutputStream Salida;
    File Archivo;
    File Tokens;
    String total = "";
    
/* METODO PARA ABRIR ARCHIVOS */
    public String AbrirArchivo(File archivo){
        String contenido = "";
        try{
            Entrada = new FileInputStream(archivo);
            int ascci;
            while((ascci = Entrada.read()) != -1){
                char carcater = (char)ascci;
                contenido += carcater;
            }
        }catch(Exception e){}
        return contenido;
    }
    
/*  METODO PARA GUARDAR ARCHIVOS */
    public String GuardarArchivo(File archivo, String contenido){
        String respuesta = null;
        try{
            Salida = new FileOutputStream(archivo);
            byte[] bytesTxt = contenido.getBytes();
            Salida.write(bytesTxt);
            respuesta = "El archivo se ha guardado con éxito";
        }catch(Exception e){}
        return respuesta;
    }
    
/* METODO PARA ESCRITURA POR RECONOCIMIENTO DE TOKENS */
    public void Tokens(String alfa) throws IOException{
        total += alfa + "\n";
         
    }
}
